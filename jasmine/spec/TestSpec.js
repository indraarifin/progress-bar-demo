describe("addValues", function() {
  it("should add numbers", function() {
    expect(addValues(25, -25)).toEqual(0);
    expect(addValues(25, -10)).toEqual(15);
    expect(addValues(50, 10)).toEqual(60);
    expect(addValues(75, 25)).toEqual(100);
    expect(addValues(100, 25)).toEqual(125);
  });
});

describe("limitProgressValue", function() {
  it("should return 0 for negative number", function() {
    expect(limitProgressValue(-10)).toEqual(0);
  });
  it("should return 0 for zero", function() {
    expect(limitProgressValue(0)).toEqual(0);
  });
  it("should return the same number for positive number", function() {
    expect(limitProgressValue(10)).toEqual(10);
  });
});