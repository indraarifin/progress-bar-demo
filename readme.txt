Progress Bar Demo
Using RactivJS
Demo video: http://scott.maclure.com.au/test/progress-bars-demo.ogv
Feature overview:
	•	Multiple bars
	•	One set of controls that can control each bar on the fly
	•	Can't go under 0
	•	Can go over 100, but limit the bar itself and change its colour
	•	Display usage amount, centered
	•	Responsive solution: mobile, tablet, etc.
	•	Bar change animation, works well when you tap buttons quickly.
