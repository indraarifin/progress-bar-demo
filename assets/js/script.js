var bars = [
    {name: '#progress1', value: 25, progress: 25},
    {name: '#progress2', value: 50, progress: 50},
    {name: '#progress3', value: 75, progress: 75}
];

var ractive = new Ractive({
    el: 'container',
    template: '#template',
    data: { progressbars: bars, selectedBarIndex: 0 }
});

ractive.on({
	updateBar: function(event, value){
		updateProgressBar(value);
	},
	selectBar: function(event, id){
		selectProgressBar(id);
	}
});

// Progress Bar Update function
function updateProgressBar(value) { 
    
    var selectedBarIndex = ractive.get('selectedBarIndex'),
        curval = ractive.get('progressbars[' + selectedBarIndex + '].value'),
        addval = parseInt(value, 10),
        newval = limitProgressValue( addValues (curval, addval));
    
    ractive.set('progressbars[' + selectedBarIndex + '].value', newval);
    ractive.animate('progressbars[' + selectedBarIndex + '].progress', newval, {duration: 100});
};

function addValues(a, b) {
	return a + b;
}

// Limit progress bar to not be lower than 0
function limitProgressValue(val) {
    var lowerlimit = 0;
    return Math.max(val, lowerlimit);
}

// Select progress bar on click
function selectProgressBar(id) {
    ractive.set('selectedBarIndex', id);
}